package com.kependudukan.ui.main.akta_kelahiran;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.kependudukan.R;
import com.kependudukan.model.response_data.DataPend;
import com.kependudukan.service.ApiService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.kependudukan.R.string.text_error_get;
import static com.kependudukan.utils.StringUtils.convertHundred;

@SuppressLint("CheckResult")
public class AktaKelahiranPresenter {

    final AktaKelahiranCallback callback;
    final Context context;
    final ApiService apiService;

    public AktaKelahiranPresenter(AktaKelahiranCallback callback, Context context, ApiService apiService) {
        this.callback = callback;
        this.context = context;
        this.apiService = apiService;
    }

    public void postAktaKelahiran(String name, String ktp, int umur, String job, String alamat,
                                  String hubungan, String tanggalKelahiran, String tempatKelahiran,
                                  String jk, String anakKe) {
        callback.onLoading(true);
        apiService.postAktaKelahiran(name, ktp, umur, job, alamat, hubungan, tanggalKelahiran,
                tempatKelahiran, jk, anakKe)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    callback.onLoading(false);
                    if (data.getSuccess()) callback.onSuccess(context.getString(R.string.text_success));
                    else callback.onError(context.getString(R.string.text_error_add));
                }, throwable -> {
                    Log.e("error_akta_kematian", throwable.getLocalizedMessage());
                    callback.onLoading(false);
                    callback.onError(context.getString(R.string.text_error_add));
                });
    }

    void getData(String ktp) {
        callback.onLoading(true);
        apiService.getData(ktp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    callback.onLoading(false);
                    if (data.getSuccess()) {
                        for (DataPend item : data.getData()) {
                            if (item.getDesc().toLowerCase().trim().replaceAll(" ", "").contains("aktakelahiran")) {
                                if (!item.getValue().isEmpty())
                                    callback.onSuccessGet(item);
                                else
                                    callback.onError(context.getString(R.string.text_error_add));
                            }
                        }
                    } else
                        callback.onError(context.getString(R.string.text_error_add));
                }, throwable -> {
                    Log.e("error_get_data", throwable.getLocalizedMessage());
                    callback.onLoading(false);
                    callback.onError(context.getString(R.string.text_error_add));
                });
    }
}
