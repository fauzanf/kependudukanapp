package com.kependudukan.ui.main.akta_kelahiran;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kependudukan.R;
import com.kependudukan.model.response_data.DataPend;
import com.kependudukan.service.ApiService;
import com.kependudukan.service.ApiUtils;
import com.kependudukan.utils.SharedPref;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.kependudukan.R.string.text_female;
import static com.kependudukan.R.string.text_male;

@SuppressLint("NonConstantResourceId")
public class AktaKelahiranActivity extends AppCompatActivity implements AktaKelahiranCallback{

    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.et_name)
    EditText editName;
    @BindView(R.id.et_ktp)
    EditText editKTP;
    @BindView(R.id.et_age)
    EditText editAge;
    @BindView(R.id.et_job)
    EditText editJob;
    @BindView(R.id.et_address)
    EditText editAddress;
    @BindView(R.id.et_relation)
    EditText editRelation;
    @BindView(R.id.et_day)
    EditText editDay;
    @BindView(R.id.et_birth_date)
    EditText editBirthDate;
    @BindView(R.id.et_time)
    EditText editTime;
    @BindView(R.id.et_place)
    EditText editPlace;
    @BindView(R.id.et_child)
    EditText editChild;

    AktaKelahiranPresenter aktaKelahiranPresenter;
    ApiService apiService;
    SharedPref pref;

    String jk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akta_kelahiran);
        ButterKnife.bind(this);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Permohonan Akta Kelahiran");

        apiService = ApiUtils.getAPIService();
        aktaKelahiranPresenter = new AktaKelahiranPresenter(this, this, apiService);
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPref.myPref, Context.MODE_PRIVATE);
        pref = new SharedPref(sharedPreferences);
    }

    @Override
    public void onLoading(boolean loading) {
        if (loading) progressBar.setVisibility(View.VISIBLE);
        else progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSuccess(String msg) {
        pref.saveAKELNumber();
        int count = pref.getAKELNumber();
        String countStr = "";
        if (count >= 0 && count < 10) countStr = "00" + count;
        else if (count >= 10 && count < 100) countStr = "0" + count;
        else countStr = String.valueOf(count);

        aktaKelahiranPresenter.getData(editKTP.getText().toString());
    }

    @Override
    public void onSuccessGet(DataPend data) {
        int count = Integer.parseInt(data.getValue().get(0));
        String countStr = "";
        if (count >= 0 && count < 10) countStr = "00" + count;
        else if (count >= 10 && count < 100) countStr = "0" + count;
        else countStr = String.valueOf(count);

        Toast.makeText(this, "Id Permohonan Akta Kelahiran : " + countStr, Toast.LENGTH_LONG).show();
        finish();
    }

    @OnClick({R.id.btn_submit, R.id.rb_male, R.id.rb_female})
    void onSubmit(View view) {
        switch (view.getId()) {
            case R.id.rb_male:
                jk = String.valueOf(this.getString(text_male));
                break;
            case R.id.rb_female:
                jk = String.valueOf(this.getString(text_female));
                break;
            case R.id.btn_submit:
                aktaKelahiranPresenter.postAktaKelahiran(editName.getText().toString(), editKTP.getText().toString(),
                        Integer.parseInt(editAge.getText().toString()), editJob.getText().toString(), editAddress.getText().toString(),
                        editRelation.getText().toString(), editBirthDate.getText().toString(), editPlace.getText().toString(),
                        jk, editChild.getText().toString());
                break;
        }
    }
}