package com.kependudukan.ui.main.akta_nikah;

import com.kependudukan.model.response_data.DataPend;

public interface AktaNikahCallback {
    void onLoading(boolean loading);

    void onError(String msg);

    void onSuccess(String msg);

    void onSuccessGet(DataPend data);
}
