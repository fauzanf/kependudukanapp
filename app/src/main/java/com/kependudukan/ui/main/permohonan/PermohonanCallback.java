package com.kependudukan.ui.main.permohonan;

import com.kependudukan.model.response_data.DataPend;

import java.util.List;

public interface PermohonanCallback {
    void onLoading(boolean status);

    void onError(String msg);

    void onSuccess(List<DataPend> data);
}
