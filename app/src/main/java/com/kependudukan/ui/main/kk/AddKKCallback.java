package com.kependudukan.ui.main.kk;

import com.kependudukan.model.response_data.DataPend;

public interface AddKKCallback {
    void onLoading(boolean loading);

    void onError(String msg);

    void onSuccess(String msg);

    void onSuccessGet(DataPend data);
}
