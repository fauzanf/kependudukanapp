package com.kependudukan.ui.main.kk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.kependudukan.R;
import com.kependudukan.model.response_data.DataPend;
import com.kependudukan.service.ApiService;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.kependudukan.R.string.text_success;

@SuppressLint("CheckResult")
public class AddKKPresenter {

    private final AddKKCallback addKKCallback;
    private final Context context;
    private final ApiService apiService;

    public AddKKPresenter(AddKKCallback addKKCallback, Context context, ApiService apiService) {
        this.addKKCallback = addKKCallback;
        this.context = context;
        this.apiService = apiService;
    }

    public void postKK(String name, String place, String jk, String job, String ktp, String negara,
                       String agama, String alamat, String birthDate) {
        addKKCallback.onLoading(true);
        apiService
                .postKK(name, place, jk, job, ktp, negara, agama, alamat, birthDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( data -> {
                    Log.d("data_kk", data.getMessage());
                    addKKCallback.onLoading(false);
                    if (data.getSuccess()) addKKCallback.onSuccess(context.getString(text_success));
                    else addKKCallback.onError(context.getString(R.string.text_error_add));
                }, throwable -> {
                    Log.e("error_add_kk", throwable.getLocalizedMessage());
                    addKKCallback.onLoading(false);
                    addKKCallback.onError(context.getString(R.string.text_error_add));
                });

    }

    void getData(String ktp) {
        addKKCallback.onLoading(true);
        apiService.getData(ktp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    addKKCallback.onLoading(false);
                    if (data.getSuccess()) {
                        for (DataPend item : data.getData()) {
                            if (item.getDesc().toLowerCase().trim().replaceAll(" ", "").contains("kartukeluarga")) {
                                if (!item.getValue().isEmpty())
                                    addKKCallback.onSuccessGet(item);
                                else
                                    addKKCallback.onError(context.getString(R.string.text_error_add));
                            }
                        }
                    } else
                        addKKCallback.onError(context.getString(R.string.text_error_add));
                }, throwable -> {
                    Log.e("error_get_data", throwable.getLocalizedMessage());
                    addKKCallback.onLoading(false);
                    addKKCallback.onError(context.getString(R.string.text_error_add));
                });
    }
}
