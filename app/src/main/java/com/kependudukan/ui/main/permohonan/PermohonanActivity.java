package com.kependudukan.ui.main.permohonan;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kependudukan.R;
import com.kependudukan.model.response_data.DataPend;
import com.kependudukan.service.ApiService;
import com.kependudukan.service.ApiUtils;
import com.kependudukan.utils.SharedPref;
import com.kependudukan.utils.StringUtils;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.kependudukan.utils.StringUtils.convertHundred;

@SuppressLint("NonConstantResourceId")
public class PermohonanActivity extends AppCompatActivity implements PermohonanCallback {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.et_ktp)
    EditText editKtp;
    @BindView(R.id.tv_kk)
    TextView tvKK;
    @BindView(R.id.tv_akel)
    TextView tvAKEL;
    @BindView(R.id.tv_an)
    TextView tvAktaNikah;
    @BindView(R.id.tv_akem)
    TextView tvAKEM;
    @BindView(R.id.tv_pe)
    TextView tvPengaduan;
    @BindView(R.id.tv_dom)
    TextView tvDOM;

    PermohonanPresenter permohonanPresenter;
    ApiService apiService;
    SharedPref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permohonan);
        ButterKnife.bind(this);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Permohonan");

        apiService = ApiUtils.getAPIService();
        permohonanPresenter = new PermohonanPresenter(this, this, apiService);
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPref.myPref, Context.MODE_PRIVATE);
        pref = new SharedPref(sharedPreferences);
    }

    @Override
    public void onLoading(boolean status) {
        if (status) progressBar.setVisibility(View.VISIBLE);
        else progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSuccess(List<DataPend> data) {
        setData(data);
    }

    @SuppressLint("SetTextI18n")
    void setData(List<DataPend> data) {
        for (DataPend item : data) {

            switch (item.getDesc().toLowerCase().trim().replaceAll(" ", "")) {
                case "kartukeluarga":
                    if (!item.getValue().isEmpty()) {
                        tvKK.setText(": " + convertHundred(item.getValue().get(0)));
                    }
                    break;
                case "aktakelahiran":
                    if (!item.getValue().isEmpty()) {
                        tvAKEL.setText(": " + convertHundred(item.getValue().get(0)));
                    }
                    break;
                case "aktakematian":
                    if (!item.getValue().isEmpty()) {
                        tvAKEM.setText(": " + convertHundred(item.getValue().get(0)));
                    }
                    break;
                case "aktanikah":
                    if (!item.getValue().isEmpty()) {
                        tvAktaNikah.setText(": " + convertHundred(item.getValue().get(0)));
                    }
                    break;
                case "permohonandomisili":
                    if (!item.getValue().isEmpty()) {
                        tvDOM.setText(": " + convertHundred(item.getValue().get(0)));
                    }
                    break;
                case "pengaduan":
                    if (!item.getValue().isEmpty()) {
                        tvPengaduan.setText(": " + convertHundred(item.getValue().get(0)));
                    }
                    break;
            }
        }
    }

    @OnClick(R.id.btn_search)
    void onClick(View view) {
        tvKK.setText("");
        tvAKEL.setText("");
        tvAktaNikah.setText("");
        tvAKEM.setText("");
        tvPengaduan.setText("");
        tvDOM.setText("");
        permohonanPresenter.getData(editKtp.getText().toString() + "");
    }
}