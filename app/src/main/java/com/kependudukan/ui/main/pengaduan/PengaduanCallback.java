package com.kependudukan.ui.main.pengaduan;

import com.kependudukan.model.response_data.DataPend;

public interface PengaduanCallback {
    void onLoading(boolean loading);

    void onError(String msg);

    void onSuccess(String msg);

    void onSuccessGet(DataPend data);
}
