package com.kependudukan.ui.main.akta_kematian;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.kependudukan.R;
import com.kependudukan.model.response_data.DataPend;
import com.kependudukan.service.ApiService;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;

@SuppressLint("CheckResult")
public class AktaKematianPresenter {
    final AktaKematianCallback callback;
    final Context context;
    final ApiService apiService;

    public AktaKematianPresenter(AktaKematianCallback callback, Context context, ApiService apiService) {
        this.callback = callback;
        this.context = context;
        this.apiService = apiService;
    }

    public void postAktaKematian(String name, String ktp, int umur, String job, String alamat,
                                 String hubungan, String tanggalKematian, String tempatKematian,
                                 String penyebabKematian, String buktiKematian) {
        callback.onLoading(true);
        apiService.postAktaKematian(name, ktp, umur, job, alamat, hubungan, tanggalKematian,
                tempatKematian, penyebabKematian, buktiKematian)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    callback.onLoading(false);
                    if (data.getSuccess()) callback.onSuccess(context.getString(R.string.text_success));
                    else callback.onError(context.getString(R.string.text_error_add));
                }, throwable -> {
                    Log.e("error_akta_kematian", throwable.getLocalizedMessage());
                    callback.onLoading(false);
                    callback.onError(context.getString(R.string.text_error_add));
                });
    }

    void getData(String ktp) {
        callback.onLoading(true);
        apiService.getData(ktp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    callback.onLoading(false);
                    if (data.getSuccess()) {
                        for (DataPend item : data.getData()) {
                            if (item.getDesc().toLowerCase().trim().replaceAll(" ", "").contains("aktakematian")) {
                                if (!item.getValue().isEmpty())
                                    callback.onSuccessGet(item);
                                else
                                    callback.onError(context.getString(R.string.text_error_add));
                            }
                        }
                    } else
                        callback.onError(context.getString(R.string.text_error_add));
                }, throwable -> {
                    Log.e("error_get_data", throwable.getLocalizedMessage());
                    callback.onLoading(false);
                    callback.onError(context.getString(R.string.text_error_add));
                });
    }
}
