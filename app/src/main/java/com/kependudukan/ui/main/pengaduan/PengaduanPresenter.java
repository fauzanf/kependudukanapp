package com.kependudukan.ui.main.pengaduan;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.kependudukan.R;
import com.kependudukan.model.response_data.DataPend;
import com.kependudukan.service.ApiService;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@SuppressLint("CheckResult")
public class PengaduanPresenter {
    final PengaduanCallback callback;
    final Context context;
    final ApiService apiService;

    public PengaduanPresenter(PengaduanCallback callback, Context context, ApiService apiService) {
        this.callback = callback;
        this.context = context;
        this.apiService = apiService;
    }

    public void postPengaduan(String name, String ktp, String jk, String alamat, String aduan) {
        callback.onLoading(true);
        apiService.postPengaduan(name, ktp, jk, alamat, aduan)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    callback.onLoading(false);
                    if (data.getSuccess()) callback.onSuccess(context.getString(R.string.text_success));
                    else callback.onError(context.getString(R.string.text_error_add));
                }, throwable -> {
                    Log.e("error_pengaduan", throwable.getLocalizedMessage());
                    callback.onLoading(false);
                    callback.onError(context.getString(R.string.text_error_add));
                });
    }

    void getData(String ktp) {
        callback.onLoading(true);
        apiService.getData(ktp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    callback.onLoading(false);
                    if (data.getSuccess()) {
                        for (DataPend item : data.getData()) {
                            if (item.getDesc().toLowerCase().trim().replaceAll(" ", "").contains("pengaduan")) {
                                if (!item.getValue().isEmpty())
                                    callback.onSuccessGet(item);
                                else
                                    callback.onError(context.getString(R.string.text_error_add));
                            }
                        }
                    } else
                        callback.onError(context.getString(R.string.text_error_add));
                }, throwable -> {
                    Log.e("error_get_data", throwable.getLocalizedMessage());
                    callback.onLoading(false);
                    callback.onError(context.getString(R.string.text_error_add));
                });
    }
}
