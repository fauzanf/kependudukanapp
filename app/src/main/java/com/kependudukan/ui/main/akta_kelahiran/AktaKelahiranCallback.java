package com.kependudukan.ui.main.akta_kelahiran;

import com.kependudukan.model.response_data.DataPend;

public interface AktaKelahiranCallback {
    void onLoading(boolean loading);

    void onError(String msg);

    void onSuccess(String msg);

    void onSuccessGet(DataPend data);
}
