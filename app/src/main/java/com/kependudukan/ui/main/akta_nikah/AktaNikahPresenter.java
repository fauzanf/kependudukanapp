package com.kependudukan.ui.main.akta_nikah;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.kependudukan.R;
import com.kependudukan.model.response_data.DataPend;
import com.kependudukan.service.ApiService;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;

@SuppressLint("CheckResult")
public class AktaNikahPresenter {

    final AktaNikahCallback callback;
    final Context context;
    final ApiService apiService;

    public AktaNikahPresenter(AktaNikahCallback callback, Context context, ApiService apiService) {
        this.callback = callback;
        this.context = context;
        this.apiService = apiService;
    }

    public void postAktaNikah(String name, String place, String birthDate, String jk, String job, String ktp,
                              String agama, String alamat) {
        callback.onLoading(true);
        apiService
                .postAktaNikah(name, place, birthDate, jk, job, ktp, agama, alamat)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    callback.onLoading(false);
                    if (data.getSuccess()) callback.onSuccess(context.getString(R.string.text_success));
                    else callback.onError(context.getString(R.string.text_error_add));
                }, throwable -> {
                    Log.e("error_add_akta_nikah", throwable.getLocalizedMessage());
                    callback.onLoading(false);
                    callback.onError(context.getString(R.string.text_error_add));
                });
    }

    void getData(String ktp) {
        callback.onLoading(true);
        apiService.getData(ktp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    callback.onLoading(false);
                    if (data.getSuccess()) {
                        for (DataPend item : data.getData()) {
                            if (item.getDesc().toLowerCase().trim().replaceAll(" ", "").contains("aktanikah")) {
                                if (!item.getValue().isEmpty())
                                    callback.onSuccessGet(item);
                                else
                                    callback.onError(context.getString(R.string.text_error_add));
                            }
                        }
                    } else
                        callback.onError(context.getString(R.string.text_error_add));
                }, throwable -> {
                    Log.e("error_get_data", throwable.getLocalizedMessage());
                    callback.onLoading(false);
                    callback.onError(context.getString(R.string.text_error_add));
                });
    }
}
