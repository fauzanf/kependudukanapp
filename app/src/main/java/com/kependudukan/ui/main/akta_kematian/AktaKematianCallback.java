package com.kependudukan.ui.main.akta_kematian;

import com.kependudukan.model.response_data.DataPend;

public interface AktaKematianCallback {
    void onLoading(boolean loading);

    void onError(String msg);

    void onSuccess(String msg);

    void onSuccessGet(DataPend data);
}
