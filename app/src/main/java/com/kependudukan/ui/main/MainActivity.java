package com.kependudukan.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.kependudukan.R;
import com.kependudukan.R2;
import com.kependudukan.ui.main.akta_kelahiran.AktaKelahiranActivity;
import com.kependudukan.ui.main.akta_kematian.AktaKematianActivity;
import com.kependudukan.ui.main.akta_nikah.AktaNikahActivity;
import com.kependudukan.ui.main.domisili.DomisiliActivity;
import com.kependudukan.ui.main.kk.AddKKActivity;
import com.kependudukan.ui.main.pengaduan.PengaduanActivity;
import com.kependudukan.ui.main.permohonan.PermohonanActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("NonConstantResourceId")
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.cv_kk, R.id.cv_akta_nikah, R.id.cv_akta_kematian, R.id.cv_pengaduan, R.id.cv_akta_kelahiran,
            R.id.cv_domisili, R.id.cv_permohonan})
    void onItemClicked(View view) {
        switch (view.getId()) {
            case R.id.cv_kk:
                startActivity(new Intent(this, AddKKActivity.class));
                break;
            case R.id.cv_akta_nikah:
                startActivity(new Intent(this, AktaNikahActivity.class));
                break;
            case R.id.cv_akta_kematian:
                startActivity(new Intent(this, AktaKematianActivity.class));
                break;
            case R.id.cv_pengaduan:
                startActivity(new Intent(this, PengaduanActivity.class));
                break;
            case R.id.cv_akta_kelahiran:
                startActivity(new Intent(this, AktaKelahiranActivity.class));
                break;
            case R.id.cv_domisili:
                startActivity(new Intent(this, DomisiliActivity.class));
                break;
            case R.id.cv_permohonan:
                startActivity(new Intent(this, PermohonanActivity.class));
                break;
        }
    }
}