package com.kependudukan.ui.main.permohonan;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.kependudukan.R;
import com.kependudukan.service.ApiService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.kependudukan.R.string.text_error_get;

@SuppressLint("CheckResult")
public class PermohonanPresenter {

    final PermohonanCallback callback;
    final Context context;
    final ApiService apiService;

    public PermohonanPresenter(PermohonanCallback callback, Context context, ApiService apiService) {
        this.callback = callback;
        this.context = context;
        this.apiService = apiService;
    }

    void getData(String ktp) {
        callback.onLoading(true);
        apiService.getData(ktp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    callback.onLoading(false);
                    if (data.getSuccess()) callback.onSuccess(data.getData());
                    else callback.onError(context.getString(text_error_get));
                }, throwable -> {
                    Log.e("error_get_data", throwable.getLocalizedMessage());
                    callback.onLoading(false);
                    callback.onError(context.getString(text_error_get));
                });
    }
}
