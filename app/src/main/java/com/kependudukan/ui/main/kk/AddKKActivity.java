package com.kependudukan.ui.main.kk;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.kependudukan.R;
import com.kependudukan.model.response_data.DataPend;
import com.kependudukan.service.ApiService;
import com.kependudukan.service.ApiUtils;
import com.kependudukan.utils.SharedPref;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

import static com.kependudukan.R.string.text_male;
import static com.kependudukan.R.string.text_success;
import static com.kependudukan.R.string.text_female;

@SuppressLint("NonConstantResourceId")
public class AddKKActivity extends AppCompatActivity implements AddKKCallback {

    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.et_name)
    EditText editName;
    @BindView(R.id.et_place)
    EditText editPlace;
    @BindView(R.id.et_birth_date)
    EditText editBirthDate;
    @BindView(R.id.rg_jk)
    RadioGroup radioJK;
    @BindView(R.id.et_job)
    EditText editJob;
    @BindView(R.id.et_ktp)
    EditText editKTP;
    @BindView(R.id.et_citizenship)
    EditText editCitizen;
    @BindView(R.id.et_religion)
    EditText editReligion;
    @BindView(R.id.et_address)
    EditText editAddress;

    ApiService apiService;
    AddKKPresenter addKKPresenter;
    SharedPref pref;
    String jk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_k_k);
        ButterKnife.bind(this);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Permohonan Kartu Keluarga");
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPref.myPref, Context.MODE_PRIVATE);
        pref = new SharedPref(sharedPreferences);

        apiService = ApiUtils.getAPIService();
        addKKPresenter = new AddKKPresenter(this, this, apiService);
    }

    @Override
    public void onLoading(boolean loading) {
        if (loading) progressBar.setVisibility(View.VISIBLE);
        else progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSuccess(String msg) {
        pref.saveKKNumber();
        int count = pref.getKKNumber();
        String countStr = "";
        if (count >= 0 && count < 10) countStr = "00" + count;
        else if (count >= 10 && count < 100) countStr = "0" + count;
        else countStr = String.valueOf(count);

        addKKPresenter.getData(editKTP.getText().toString());
    }

    @Override
    public void onSuccessGet(DataPend data) {
        int count = Integer.parseInt(data.getValue().get(0));
        String countStr = "";
        if (count >= 0 && count < 10) countStr = "00" + count;
        else if (count >= 10 && count < 100) countStr = "0" + count;
        else countStr = String.valueOf(count);

        Toast.makeText(this, "Id Permohonan KK : " + countStr, Toast.LENGTH_LONG).show();
        finish();
    }

    @OnClick({R.id.btn_submit, R.id.rb_male, R.id.rb_female})
    void onSubmit(View view) {
        switch (view.getId()) {
            case R.id.rb_male:
                jk = String.valueOf(this.getString(text_male));
                break;
            case R.id.rb_female:
                jk = String.valueOf(this.getString(text_female));
                break;
            case R.id.btn_submit:
                addKKPresenter.postKK(editName.getText().toString(), editPlace.getText().toString(),
                        jk, editJob.getText().toString(), editKTP.getText().toString(),
                        editCitizen.getText().toString(), editReligion.getText().toString(),
                        editAddress.getText().toString(), editBirthDate.getText().toString());
                break;
        }
    }
}