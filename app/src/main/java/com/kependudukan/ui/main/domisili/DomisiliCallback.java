package com.kependudukan.ui.main.domisili;

import com.kependudukan.model.response_data.DataPend;

public interface DomisiliCallback {
    void onLoading(boolean loading);

    void onError(String msg);

    void onSuccess(String msg);

    void onSuccessGet(DataPend data);
}
