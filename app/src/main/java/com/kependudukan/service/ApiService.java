package com.kependudukan.service;


import com.kependudukan.model.PendResponse;
import com.kependudukan.model.response_data.DataPend;
import com.kependudukan.model.response_data.ResponsePend;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @POST("api/v1/kartu-keluarga/create.php")
    @FormUrlEncoded
    Observable<PendResponse> postKK(@Field("nama") String name,
                                    @Field("tempat_lahir") String place,
                                    @Field("jenis_kelamin") String jk,
                                    @Field("pekerjaan") String job,
                                    @Field("ktp") String ktp,
                                    @Field("negara") String negara,
                                    @Field("agama") String agama,
                                    @Field("alamat") String alamat,
                                    @Field("tanggal_lahir") String birthDate);

    @POST("api/v1/akta-nikah/create.php")
    @FormUrlEncoded
    Observable<PendResponse> postAktaNikah(@Field("nama") String name,
                                           @Field("tempat_lahir") String place,
                                           @Field("tanggal_lahir") String birthDate,
                                           @Field("jenis_kelamin") String jk,
                                           @Field("pekerjaan") String job,
                                           @Field("ktp") String ktp,
                                           @Field("agama") String agama,
                                           @Field("alamat") String alamat);

    @POST("api/v1/akta-kematian/create.php")
    @FormUrlEncoded
    Observable<PendResponse> postAktaKematian(@Field("nama") String name,
                                              @Field("ktp") String ktp,
                                              @Field("umur") int umur,
                                              @Field("pekerjaan") String job,
                                              @Field("alamat") String alamat,
                                              @Field("hubungan") String hubungan,
                                              @Field("tanggal_kematian") String tanggalKematian,
                                              @Field("tempat_kematian") String tempatKematian,
                                              @Field("penyebab_kematian") String penyebabKematian,
                                              @Field("bukti_kematian") String buktiKematian);

    @POST("api/v1/pengaduan/create.php")
    @FormUrlEncoded
    Observable<PendResponse> postPengaduan(@Field("nama") String name,
                                           @Field("ktp") String ktp,
                                           @Field("jenis_kelamin") String jk,
                                           @Field("alamat") String alamat,
                                           @Field("aduan") String aduan);

    @POST("api/v1/akta-kelahiran/create.php")
    @FormUrlEncoded
    Observable<PendResponse> postAktaKelahiran(@Field("nama") String name,
                                              @Field("ktp") String ktp,
                                              @Field("umur") int umur,
                                              @Field("pekerjaan") String job,
                                              @Field("alamat") String alamat,
                                              @Field("hubungan") String hubungan,
                                              @Field("tanggal_lahir") String tanggalLahir,
                                              @Field("tempat_lahir") String tempatKematian,
                                              @Field("jenis_kelamin") String jk,
                                              @Field("anak_ke") String anakKe);

    @POST("api/v1/domisili/create.php")
    @FormUrlEncoded
    Observable<PendResponse> postDomisili(@Field("nama") String name,
                                          @Field("tempat_lahir") String place,
                                          @Field("tanggal_lahir") String birthDate,
                                          @Field("pekerjaan") String job,
                                          @Field("agama") String agama,
                                          @Field("status_perkawinan") String marriedStatus,
                                          @Field("kewarganegaraan") String kewarganegaraan,
                                          @Field("ktp") String ktp,
                                          @Field("alamat") String alamat);

    @GET("api/v1/permohonan/index.php")
    Observable<ResponsePend> getData(@Query("ktp") String ktp);
}
