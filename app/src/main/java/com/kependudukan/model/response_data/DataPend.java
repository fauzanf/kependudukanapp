package com.kependudukan.model.response_data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataPend {
    @SerializedName("value")
    @Expose
    private List<String> value = null;
    @SerializedName("desc")
    @Expose
    private String desc;

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}