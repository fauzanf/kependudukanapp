package com.kependudukan.utils;

public class StringUtils {
    public static String convertHundred(String value) {
        if (value != null && value != "") {
            int number = Integer.parseInt(value);
            if (number >= 0 && number < 10) return "00" + number;
            else if (number >= 10 && number < 100) return "0" + number;
            else return String.valueOf(number);
        } else return "";
    }
}
