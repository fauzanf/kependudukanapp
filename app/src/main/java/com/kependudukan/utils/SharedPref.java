package com.kependudukan.utils;

import android.content.SharedPreferences;

public class SharedPref {
    final SharedPreferences sharedPreferences;
    public static final String myPref = "kependudukan";
    public static String COUNT_KK = "countKK";
    public static String COUNT_AN = "countAN";
    public static String COUNT_AKEM = "countAKEM";
    public static String COUNT_AKEL = "countAKEL";
    public static String COUNT_PENGADUAN = "countPengaduan";
    public static String COUNT_DOMISILI = "countDomisili";

    public SharedPref(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    //save nomer urutan permohonan kartu keluarga
    public void saveKKNumber() {
        int count = sharedPreferences.getInt(COUNT_KK, 0);
        count = count + 1;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(COUNT_KK, count);
        editor.apply();
    }

    public int getKKNumber() {
        return sharedPreferences.getInt(COUNT_KK, 1);
    }

    //save nomer urutan permohonan akta nikah
    public void saveANNumber() {
        int count = sharedPreferences.getInt(COUNT_AN, 0);
        count = count + 1;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(COUNT_AN, count);
        editor.apply();
    }

    public int getANNumber() {
        return sharedPreferences.getInt(COUNT_AN, 1);
    }

    //save nomer urutan permohonan akta kematian
    public void saveAKEMNumber() {
        int count = sharedPreferences.getInt(COUNT_AKEM, 0);
        count = count + 1;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(COUNT_AKEM, count);
        editor.apply();
    }

    public int getAKEMNumber() {
        return sharedPreferences.getInt(COUNT_AKEM, 1);
    }

    //save nomer urutan pengaduan
    public void savePengaduanNumber() {
        int count = sharedPreferences.getInt(COUNT_PENGADUAN, 0);
        count = count + 1;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(COUNT_PENGADUAN, count);
        editor.apply();
    }

    public int getPengaduanNumber() {
        return sharedPreferences.getInt(COUNT_PENGADUAN, 1);
    }

    //save nomer urutan permohonan akta kelahiran
    public void saveAKELNumber() {
        int count = sharedPreferences.getInt(COUNT_AKEL, 0);
        count = count + 1;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(COUNT_AKEL, count);
        editor.apply();
    }

    public int getAKELNumber() {
        return sharedPreferences.getInt(COUNT_AKEL, 1);
    }

    //save nomer urutan permohonan Domisili
    public void saveDomNumber() {
        int count = sharedPreferences.getInt(COUNT_DOMISILI, 0);
        count = count + 1;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(COUNT_DOMISILI, count);
        editor.apply();
    }

    public int getDomNumber() {
        return sharedPreferences.getInt(COUNT_DOMISILI, 1);
    }
}
